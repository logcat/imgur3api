package com.invisiphoto.imgur3;

import com.invisiphoto.imgur3.model.BooleanResponse;
import com.invisiphoto.imgur3.model.ImageResponse;

import junit.framework.TestCase;

import net.iharder.Base64;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import retrofit.mime.TypedFile;

public class Imgur3ApiTest extends TestCase {

    private Imgur3Api api;

    public void setUp() {
        api = Imgur3ApiFactory.createApi();
    }

    private URL getTestImageURL() {
        return getClass().getClassLoader().getResource("test.jpg");
    }

    private File getTestImageFile() {
        return new File(getTestImageURL().getPath());
    }

    private ImageResponse uploadImageFile() {
        TypedFile imageTypedFile = new TypedFile("image/jpeg", getTestImageFile());
        return api.imageUpload(imageTypedFile).toBlocking().first();
    }

    private ImageResponse uploadBase64Image() throws IOException {
        return api.imageUpload(Base64.encodeFromFile(getTestImageURL().getFile())).toBlocking().first();
    }

    public void testFileUpload() {
        ImageResponse uploadResponse = uploadImageFile();
        assertTrue(uploadResponse.success);

        System.out.println("uploaded file image: " + uploadResponse.data.link);
    }

    public void testBase64Upload() throws IOException {
        ImageResponse uploadResponse = uploadBase64Image();
        assertTrue(uploadResponse.success);

        System.out.println("uploaded base64 image: " + uploadResponse.data.link);
    }

    public void testDelete() {
        ImageResponse uploadResponse = uploadImageFile();
        assertTrue(uploadResponse.success);

        BooleanResponse response = api.imageDelete(uploadResponse.data.deletehash).toBlocking().first();
        assertTrue(response.success);

        System.out.println("deleted image: " + uploadResponse.data.link);
    }

    public void testGet() {
        ImageResponse uploadResponse = uploadImageFile();
        assertTrue(uploadResponse.success);

        ImageResponse imageResponse = api.getImageInfo(uploadResponse.data.id).toBlocking().first();
        assertTrue(imageResponse.success);

        System.out.println("get image: " + imageResponse.data.link);
    }

}
