package com.invisiphoto.imgur3.model;

public class Image {

    public String id;
    public String name;
    public String deletehash;
    public String link;
}
