package com.invisiphoto.imgur3;

import com.invisiphoto.imgur3.model.BooleanResponse;
import com.invisiphoto.imgur3.model.Image;
import com.invisiphoto.imgur3.model.ImageResponse;

import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;
import rx.Observable;


public interface Imgur3Api {

    @GET("/image/{id}")
    Observable<ImageResponse> getImageInfo(@Path("id") String id);

    @Multipart
    @POST("/image")
    Observable<ImageResponse> imageUpload(@Part("image") TypedFile image);

    @Multipart
    @POST("/image")
    Observable<ImageResponse> imageUpload(@Part("image") String image);

    @DELETE("/image/{deletehash}")
    Observable<BooleanResponse> imageDelete(@Path("deletehash") String deletehash);

}
