package com.invisiphoto.imgur3;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import com.invisiphoto.imgur3.Imgur3Api;

import java.util.Date;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class Imgur3ApiFactory {

    public static Imgur3Api createApi() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(Date.class, new DateTypeAdapter())
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.imgur.com/3")
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(request -> request.addHeader("Authorization", "Client-ID 516c84d095e03da"))
//                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return restAdapter.create(Imgur3Api.class);
    }

}
